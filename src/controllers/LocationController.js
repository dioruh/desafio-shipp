const Store = require("../models/Store");
const Location = require("../models/Location");
const Log = require("../models/Log");
const haversine = require("haversine");

const add = async (req, res) => {
  try {
    const { store_id } = req.params;
    const {
      street_number,
      country,
      street_name,
      city,
      state,
      zip_code,
      footage,
      latitude,
      longitude
    } = req.body;

    const store = await Store.findByPk(store_id);

    if (!store) {
      return res.status(400).json({ error: "Store ID not found!" });
    }

    const location = await Location.create({
      store_id,
      country,
      street_number,
      street_name,
      city,
      state,
      zip_code,
      footage,
      latitude,
      longitude
    });

    return res.json(location);
  } catch (error) {
    return res.status(400).json({ error });
  }
};

const searchByLocation = async (req, res) => {
  if (!req.body.latitude && !req.body.longitude) {
    await Log.create({
      latitude: null,
      longitude: null,
      status_code: 400,
      returned_stores: 0
    });

    return res
      .status(400)
      .json({ error: "Please provide latitude and longitude!" });
  }
  const nearMe = [];
  let distance;
  const { latitude, longitude } = req.body;

  const locations = await Location.findAll({
    include: [
      {
        model: Store,
        as: "store",
        required: true
      }
    ]
  });

  locations.forEach(data => {
    const start = { latitude, longitude };
    const end = { latitude: data.latitude, longitude: data.longitude };
    distance = haversine(start, end);

    if (distance < 6.5) {
      const payload = data.toJSON();
      nearMe.push({ distance: `${distance.toFixed(2)}KM`, ...payload });
    }
  });

  await Log.create({
    latitude,
    longitude,
    status_code: 200,
    returned_stores: nearMe.length
  });

  return res.json(nearMe);
};

const index = async (req, res) => {
  const { store_id } = req.params;

  const store = await Store.findByPk(store_id, {
    include: { association: "locations" }
  });

  if (!store) {
    return res.status(400).json({ error: "Store ID not found!" });
  }

  return res.json(store);
};

module.exports = {
  add,
  index,
  searchByLocation
};
