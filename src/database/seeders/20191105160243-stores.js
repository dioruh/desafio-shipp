"use strict";
const csv = require("csvtojson");
const path = require("path");
const csvFilePath = path.resolve(
  __dirname,
  "..",
  "..",
  "config",
  "dataset.csv"
);
const headers = [
  "country",
  "license_number",
  "operation_type",
  "establishment_type",
  "entity_name",
  "dba_name",
  "street_number",
  "street_name",
  "address1",
  "address2",
  "city",
  "state",
  "zip_code",
  "footage",
  "location"
];
const params = {
  trim: true,
  noheader: true,
  headers
};
const items = [];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await csv(params)
      .fromFile(csvFilePath)
      .then(jsonObj => {
        jsonObj.forEach(item => {
          if (Number(item.license_number))
            items.push({
              license_number: Number(item.license_number),
              operation_type: item.operation_type,
              establishment_type: item.establishment_type,
              entity_name: item.entity_name,
              dba_name: item.dba_name,
              createdAt: new Date(),
              updatedAt: new Date()
            });
        });
      });
    return queryInterface.bulkInsert("stores", items, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("stores", null, {});
  }
};
