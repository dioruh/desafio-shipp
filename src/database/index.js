const Sequelize = require("sequelize");
const dbConfig = require("../config/database");

const Store = require("../models/Store");
const Location = require("../models/Location");
const Log = require("../models/Log");
const connection = new Sequelize(dbConfig);

Store.init(connection);
Location.init(connection);
Log.init(connection);
Location.associate(connection.models);
Store.associate(connection.models);

module.exports = connection;
