const { Model, DataTypes } = require("sequelize");

class Store extends Model {
  static init(sequelize) {
    super.init(
      {
        license_number: DataTypes.INTEGER,
        operation_type: DataTypes.STRING,
        establishment_type: DataTypes.STRING,
        entity_name: DataTypes.STRING,
        dba_name: DataTypes.STRING
      },
      { sequelize }
    );
  }

  static associate(models) {
    this.hasMany(models.Location, { foreignKey: "store_id", as: "locations" });
  }
}

module.exports = Store;
