const express = require("express");
const StoreController = require("./controllers/StoreController");
const LocationController = require("./controllers/LocationController");
const routes = express.Router();

routes.get("/V1/stores", LocationController.searchByLocation);

routes.post("/stores", StoreController.add);
routes.post("/stores/:store_id/location", LocationController.add);
routes.get("/stores/:store_id/location", LocationController.index);
routes.get("/stores", StoreController.index);

module.exports = routes;
